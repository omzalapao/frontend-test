import { useRef } from "react"
import styled from "styled-components"
import { TodoItemContainer } from "@/components/TodoItem"
import { useAppDispatch, AppDispatch, filterSelector, useAppSelector } from "@/store"
import { addTodo } from "@/store/reducers/addTodoReducer"
import { getTodoList } from "@/store/reducers/getTodoReducer"

export const TodoInput = () => {
  const formRef = useRef<HTMLFormElement>(null)
  const dispatch: AppDispatch = useAppDispatch()
  const {
    filterParams
  } = useAppSelector(filterSelector)

  const handleSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault()
    const data = new FormData(e.target)
    const _title = await data.get('title') as string
    await dispatch(addTodo({
      title: _title,
      completed: false
    }))

    await dispatch(getTodoList(filterParams))
    formRef.current?.reset()
  }

  return (
    <Form as="form" ref={formRef} onSubmit={handleSubmit}>
      <Input type="text" name="title" placeholder="Add your todo..." required />
    </Form>
  )
}

const Form = styled(TodoItemContainer)``

const Input = styled.input`
  width: 100%;
  font-size: 16px;
  border: none;
`

