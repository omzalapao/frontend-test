import { useState, useRef } from "react"
import styled from "styled-components"
import { Checkbox } from "@/components/CheckBox"
import { ItemAction } from "@/components/ItemAction"
import { useAppDispatch, AppDispatch, useAppSelector, filterSelector } from '@/store'
import { getTodoList, getTodoAll } from '@/store/reducers/getTodoReducer'
import { toggleComplete, updateTitle } from "@/store/reducers/updateTodoReducer"
import type { TodoData } from "@/types"

interface ITodoItem {
  data: TodoData
}

export const TodoItem = ({ data }: ITodoItem) => {
  const formRef = useRef<HTMLFormElement>(null)
  const dispatch: AppDispatch = useAppDispatch()
  const {
    filterParams
  } = useAppSelector(filterSelector)

  const [isEdit, setIsEdit] = useState(false)

  const handleToggleComplete = async () => {
    await dispatch(toggleComplete({
      id: data.id,
      completed: !data.completed,
    }))

    await dispatch(getTodoList(filterParams))
    await dispatch(getTodoAll())
  }

  const handleSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault()
    const _data = new FormData(e.target)
    const _title = await _data.get('title') as string

    await dispatch(updateTitle({
      id: data.id,
      title: _title,
    }))
    await dispatch(getTodoList({}))

    setIsEdit(false)
    formRef.current?.reset()
  }

  return (
    <>
      {!isEdit 
        ? (<TodoItemContainer>
          <Checkbox 
            label={data.title}
            id={data.id}
            name={data.id}
            checked={data.completed}
            onChange={() => handleToggleComplete()}
          />
          <ItemAction id={data.id} handleEdit={() => setIsEdit(!isEdit)} />
        </TodoItemContainer>) 
        : (<Form as="form" ref={formRef} onSubmit={handleSubmit}>
          <Input 
            type="text" 
            name="title"
            defaultValue={data.title}
          />
          <EditButton type="submit">Save</EditButton>
        </Form>)
      }
    </>
  )
}

export const TodoItemContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 46px;
  padding: 0 20px;
  border-radius: 50px;
  background-color: #fff;
  box-shadow: 0px 0px 20px 0px #00000008;
`

const EditButton = styled.button`
  width: 64px;
  height: 36px;
  border-radius: 36px;
  margin-right: -15px;
  background-color: #585292;
  color: #fff;
  border: none;
  cursor: pointer;
`

const Form = styled(TodoItemContainer)``

const Input = styled.input`
  width: 100%;
  font-size: 16px;
  border: none;
`