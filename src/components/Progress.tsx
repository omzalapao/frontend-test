import { useEffect } from "react"
import styled from "styled-components"
import { TodoData } from "@/types"
import { useAppDispatch, AppDispatch, useAppSelector, filterSelector, getTodoListSelector } from "@/store"
import { getTodoAll } from "@/store/reducers/getTodoReducer"

export const Progress = () => {
  const dispatch: AppDispatch = useAppDispatch()
  const { filterParams } = useAppSelector(filterSelector)
  const { todoAllData } = useAppSelector(getTodoListSelector)

  useEffect(() => {
    if (todoAllData.length === 0 || 'completed' in filterParams) {
      ;(async () => await dispatch(getTodoAll()))()
    }
  }, [dispatch, todoAllData, filterParams])

  const completeTask = todoAllData.filter((item: TodoData) => item.completed === true)
  const percent = (100 * completeTask.length) / todoAllData.length || 0

  return (
    <ProgessContainer>
      <h2>Progress</h2>
      <ProgressBar>
        <ProgressBarIndicator $percentage={`${percent}%`} />
      </ProgressBar>
      <p>{completeTask.length || 0} completed</p>
    </ProgessContainer>
  )
}

const ProgessContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: start;
  gap: 12px;
  width: 100%;
  padding: 24px;
  border-radius: 20px;
  background-color: #e07c7c;
  margin-bottom: 20px;

  & h2 {
    color: #fff;
    font-size: 28px;
    font-weight: 500;
  }

  & p {
    color: #ebb9b8;
    font-size: 16px;
    font-weight: 400;
  }
`

const ProgressBar = styled.div`
  position: relative;
  width: 100%;
  height: 8px;
  border-radius: 10px;
  background-color: #3b3b3b;
`

const ProgressBarIndicator = styled(ProgressBar)<{ $percentage: string }>`
  position: absolute;
  top: 0;
  left: 0;
  width: ${(p) => p.$percentage};
  background-color: #fff;
`