import styled, { css } from "styled-components"

interface ICheckbox {
  checked: boolean,
  onChange: () => void,
  name: string,
  id: string,
  label: string,
}

export const Checkbox = ({
  checked,
  onChange,
  name,
  id,
  label,
}: ICheckbox) => {
  return (
    <Label htmlFor={id}>
      <Input
        id={id}
        type="checkbox"
        name={name}
        value={id}
        checked={checked}
        onChange={onChange}
      />
      <Indicator $check={checked} />
      <Text $check={checked}>{label}</Text>
    </Label>
  )
}

const Input = styled.input`
  height: 0;
  width: 0;
  opacity: 0;
  z-index: -1;
`

const Text = styled.p<{ $check: boolean }>`
  font-size: 16px;
  font-weight: 400;
  color: #2e2e2e;
  margin-left: 12px;

  ${(p) => (p.$check 
    ? css`
        color: #a9a9a9;
        text-decoration: line-through;
      ` 
    : css`
        color: #2e2e2e;
      ` 
    )
  }
`

const Label = styled.label`
  display: flex;
  justify-content: start;
  align-items: center;
  margin: 0;
  cursor: pointer;
`

const Indicator = styled.div<{ $check?: boolean }>`
  width: 22px;
  height: 22px;
  background: ${(p) => (p.$check ? "#585292" : "transparent")};
  position: relative;
  top: 0;
  left: 0;
  border: 2px solid #585292;
  border-radius: 6px;

  &::after {
    content: "";
    position: absolute;
    display: none;
  }

  ${Input}:checked + &::after {
    display: block;
    top: 0.15em;
    left: 0.4em;
    width: 20%;
    height: 50%;
    border: 1px solid #fff;
    border-width: 0 0.1em 0.1em 0;
    transform: rotate(45deg);
  }
`