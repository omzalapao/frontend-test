import { useState } from "react"
import styled from "styled-components"
import Dots from "@/assets/dots.svg"
import { useOutsideClick } from "@/utils/hooks/useOutsideClick"
import { useAppDispatch, AppDispatch, useAppSelector, filterSelector } from "@/store"
import { deleteTodo } from "@/store/reducers/deleteTodoReducer"
import { getTodoList, getTodoAll } from "@/store/reducers/getTodoReducer"

interface IItemAction {
  id: string
  handleEdit: () => void
}

export const ItemAction = ({ id, handleEdit }: IItemAction) => {
  const dispatch: AppDispatch = useAppDispatch()
  const {
    filterParams
  } = useAppSelector(filterSelector)

  const [toggleAction, setToggleAction] = useState(false)
  const refToggleAction = useOutsideClick(() => setToggleAction(false)) as never

  const handleDelete = async () => {
    await dispatch(deleteTodo({
      id: id
    }))
    await dispatch(getTodoList(filterParams))
    await dispatch(getTodoAll())
    setToggleAction(false)
  }

  return (
    <>
      <ActionButton onClick={() => setToggleAction(!toggleAction)}>
        <img src={Dots} alt="dots" />
      </ActionButton>
      {toggleAction 
        ? (<ActionLists ref={refToggleAction}>
          <li onClick={() => { handleEdit(); setToggleAction(false) }}>Edit</li>
          <li onClick={() => handleDelete()}>Delete</li>
        </ActionLists>) 
        : null
      }
    </>
  )
}

const ActionButton = styled.button`
  display: contents;
  width: auto;
  height: auto;
  border: none;
  background-color: transparent;
  cursor: pointer;
`

const ActionLists = styled.ul`
  position: absolute;
  z-index: 100;
  bottom: -72px;
  right: 0;
  width: 100%;
  max-width: 120px;
  padding: 16px;
  list-style: none;
  border-radius: 10px;
  background-color: #fff;
  box-shadow: 0px 0px 8px 0px #0000001a;

  & li {
    color: #2e2e2e;
    margin-bottom: 10px;
    cursor: pointer;
  }
  & li:nth-child(2) {
    color: #e07c7c;
    margin-bottom: 0;
  }
`