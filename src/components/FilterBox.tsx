import { useEffect, useState } from "react"
import styled from "styled-components"
import ChevDown from "@/assets/chev-down.svg"
import { useOutsideClick } from "@/utils/hooks/useOutsideClick"
import { useAppDispatch, AppDispatch, useAppSelector, filterSelector } from "@/store"
import { getTodoList } from "@/store/reducers/getTodoReducer"
import { changeFilterKey } from "@/store/reducers/filterReducer"
import { ActiveItems, FilterParams } from "@/types"

export const FilterBox = () => {
  const dispatch: AppDispatch = useAppDispatch()
  const {
    filterKey,
    filterParams
  } = useAppSelector(filterSelector)

  const [active, setActive] = useState<ActiveItems>(filterKey)
  const [visible, setVisible] = useState(false)
  const refList = useOutsideClick(() => setVisible(false)) as never

  const handleFilter = async (key: ActiveItems, params: FilterParams) => {
    await dispatch(changeFilterKey({
      key: key,
      params: params
    }))
    setActive(key)
    setVisible(false)
  }

  useEffect(() => {
    ;(async () => {
      await dispatch(getTodoList(filterParams))
    })()
  }, [dispatch, filterParams])

  return (
    <DropDownContainer>
      <DropDownHeader onClick={() => setVisible(!visible)}>
        {active}
        <img src={ChevDown} alt="arrow" />
      </DropDownHeader>
      {visible 
        ? (<DropDownList ref={refList}>
          <DropDownListItem
            $active={active === ActiveItems.All}
            onClick={() => handleFilter(ActiveItems.All, {})}
          >
            All
          </DropDownListItem>
          <DropDownListItem
            $active={active === ActiveItems.Done}
            onClick={() => handleFilter(ActiveItems.Done, { completed: true })}
          >
            Done
          </DropDownListItem>
          <DropDownListItem
            $active={active === ActiveItems.Undone}
            onClick={() => handleFilter(ActiveItems.Undone, { completed: false })}

          >
            Undone
          </DropDownListItem>
        </DropDownList>) 
        : null
      }
    </DropDownContainer>
  )
}

const DropDownContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 110px;
`

const DropDownHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  font-size: 13px;
  padding: 6px 8px;
  border-radius: 10px;
  background-color: #fff;
  cursor: pointer;
`

const DropDownList = styled.ul`
  position: absolute;
  z-index: 100;
  bottom: -120px;
  width: 100%;
  padding: 6px 8px;
  border-radius: 10px;
  list-style: none;
  background-color: #fff;
  box-shadow: 0px 0px 8px 0px #0000001a;
`

const DropDownListItem = styled.li<{ $active: boolean }>`
  font-size: 14px;
  margin-bottom: 6px;
  padding: 6px;
  background-color: ${(p) => p.$active ? '#585292' : '#fff'};
  color: ${(p) => p.$active ? '#fff' : '#000'};
  border-radius: 8px;
  cursor: pointer;

  & :last-child {
    margin-bottom: 0;
  }
`