
export interface TodoData {
  id: string
  title: string
  completed: boolean
}

export type ToggleCompletePayload = Omit<TodoData, 'title'>

export type UpdatePayload = Omit<TodoData, 'completed'>

export type AddTodoPayload = Omit<TodoData, 'id'>

export type DeleteTodoPayload = Pick<TodoData, 'id'>

export type FilterParams = {
  completed?: boolean
}

export enum ActiveItems {
  All = 'All',
  Done = 'Done',
  Undone = 'Undone'
}