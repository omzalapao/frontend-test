import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  *{
    margin: 0;
    padding: 0;
    outline:0;
    box-sizing: border-box;
    font-family: "Roboto", sans-serif;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

  #root{
    margin:0 auto;
  }

  body {
    background-color: #d1d0d9;
  }
  
  a {
    text-decoration: none;
    cursor: pointer;
  }
`