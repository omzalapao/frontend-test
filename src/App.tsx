import { useEffect } from 'react'
import styled from 'styled-components'
import { TodoItem } from '@/components/TodoItem'
import { TodoInput } from '@/components/TodoInput'
import { FilterBox } from '@/components/FilterBox'
import { Progress } from '@/components/Progress'
import { useAppDispatch, getTodoListSelector, AppDispatch, useAppSelector, filterSelector } from '@/store'
import { getTodoList } from '@/store/reducers/getTodoReducer'
import { TodoData } from './types'
import { devices } from '@/constants/Breakpoints'

const App = () => {
  const dispatch: AppDispatch = useAppDispatch()
  const { filterParams } = useAppSelector(filterSelector)
  const { todoListData } = useAppSelector(getTodoListSelector)

  useEffect(() => {
    if (todoListData.length === 0) {
      ;(async () => await dispatch(getTodoList(filterParams)))()
    }
  }, [dispatch, todoListData, filterParams])

  return (
    <Wrapper>
      <Container>
        <TaskHeading>
          <TaskBox>
            <Progress />
            <h3>Tasks</h3>
            <FilterBox />
          </TaskBox>
        </TaskHeading>
        <TaskContent>
          {todoListData.length > 0 ? todoListData.map((item: TodoData) => <TodoItem key={item.id} data={item} />) : null}
        </TaskContent>
        <TaskInput>
          <TaskBox>
            <TodoInput />
          </TaskBox>
        </TaskInput>
      </Container>
    </Wrapper>
  )
}

export default App

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 16px 0;

  @media ${devices.md} {
    padding: 40px 0;
  }
`

const Container = styled(Wrapper)`
  width: 100%;
  max-width: 720px;
  padding: 20px;
  background-color: #f5f5f5;
  border-radius: 20px;

  @media ${devices.md} {
    padding: 60px;
  }
`

const TaskContent = styled(Wrapper)`
  position: relative;
  width: 100%;
  max-width: 512px;
  padding: 0;
  gap: 16px;
`

const TaskBox = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  max-width: 512px;
  padding: 0;
`

const TaskHeading = styled.div`
  position: sticky;
  top: 0;
  z-index: 100;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: #f5f5f5;
  padding: 12px 0;

  & h3 {
    font-size: 24px;
    color: #000;
  }
`

const TaskInput = styled.div`
  position: sticky;
  bottom: 0;
  width: 100%;
  padding: 16px 0;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: #f5f5f5;
`