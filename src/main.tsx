import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import App from '@/App.tsx'
import { GlobalStyles } from '@/GlobalStyles'
import store from '@/store'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <>
    <Provider store={store}>
      <GlobalStyles />
      <App />
    </Provider>
  </>,
)
