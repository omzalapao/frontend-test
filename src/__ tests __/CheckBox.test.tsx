import { render, screen, fireEvent } from "@testing-library/react"
import { Checkbox } from "@/components/CheckBox"

describe('Checkbox Component', () => {
  beforeEach(() => {
    const handleChange = jest.fn()
    render(<Checkbox 
      label="Test Todo"
      id="xiew6wera"
      name="xiew6wera"
      checked={false}
      onChange={() => handleChange} 
    />)
  })
  test("get label", () => {
    expect(screen.queryByRole("label"))
  })
  it("get id", () => {
    expect(screen.queryByRole("id"))
  })
  it("get name", () => {
    expect(screen.queryByRole("name"))
  })
  it("checked", () => {
    expect(screen.queryByRole("checkbox")).not.toBeChecked()
  })
  it('toggle checkbox', () => {
    fireEvent.click(screen.getByRole("checkbox"))
    expect(screen.queryByRole("checked"))
  })
})