import axios from "axios"
import type { InternalAxiosRequestConfig, AxiosResponse } from "axios"
import { API_URL } from "@/constants/ApiConstant"

declare module 'axios' {
  interface InternalAxiosRequestConfig {
    cancelPreviousRequests?: boolean
    signal?: GenericAbortSignal
  }
}

const pendingRequests: { [key: string]: AbortController } = {}

const removePendingRequest = (url: string, abort = false): void => {
  if (url && pendingRequests[url]) {
    if (abort) {
      pendingRequests[url].abort()
    }
  }
  delete pendingRequests[url]
}

const timeoutValue = 60000 //60s

const service = axios.create({
  baseURL: API_URL,
  timeout: timeoutValue
})


service.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {
    if (config?.cancelPreviousRequests && config?.url && !config.signal) {
      removePendingRequest(config.url, true)

      const abortController = new AbortController()
      config.signal = abortController.signal
      pendingRequests[config.url] = abortController
    }

    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

service.interceptors.response.use(
  (response: AxiosResponse) => {
    removePendingRequest(response.request.responseURL)
    return response.data
  },
  (error) => {
    removePendingRequest(error.config?.url)

    return Promise.reject(error)
  }
)

export default service