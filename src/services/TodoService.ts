import fetch from "@/services/FetchInterceptor"
import { AddTodoPayload, DeleteTodoPayload, ToggleCompletePayload, UpdatePayload, FilterParams } from '@/types'

const getList = (params: FilterParams) => {
  const _params = {
    _sort: 'completed',
    _order: 'desc'
  }

  if (Object.keys(params).length > 0) {
    Object.assign(_params, params)
  }

  return fetch({
    url: '/todos',
    method: 'get',
    params: _params
  })
}

const getAll = () => {
  return fetch({
    url: '/todos',
    method: 'get',
  })
}

const addTodo = (data: AddTodoPayload) => {
  const { title, completed } = data
  return fetch({
    url: `/todos`,
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    data: JSON.stringify({
      'title': title,
      'completed': completed,
    })
  })
}

const updateTodo = (data: UpdatePayload) => {
  const { id, title } = data
  return fetch({
    url: `/todos/${id}`,
    method: 'patch',
    headers: {
      'Content-Type': 'application/json',
    },
    data: JSON.stringify({
      'title': title
    })
  })
}

const toggleComplete = (data: ToggleCompletePayload) => {
  const { id, completed } = data
  return fetch({
    url: `/todos/${id}`,
    method: 'patch',
    headers: {
      'Content-Type': 'application/json',
    },
    data: JSON.stringify({
      'completed': completed
    })
  })
}

const deleteTodo = (data: DeleteTodoPayload) => {
  const { id } = data
  return fetch({
    url: `/todos/${id}`,
    method: 'delete',
  })
}

export default {
  getList,
  addTodo,
  updateTodo,
  deleteTodo,
  toggleComplete,
  getAll
}