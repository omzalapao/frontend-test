import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import TodoService from "@/services/TodoService"
import type { FilterParams, TodoData } from "@/types"

interface IGetTodoInitialState {
  todoListLoading: boolean
  todoListError: string | null
  todoListData: TodoData[] | Record<PropertyKey, never>
  todoAllLoading: boolean
  todoAllError: string | null
  todoAllData: TodoData[] | Record<PropertyKey, never>
}

export const initialState: IGetTodoInitialState = {
  todoListLoading: false,
  todoListError: null,
  todoListData: [],
  todoAllLoading: false,
  todoAllError: null,
  todoAllData: []
}

export const getTodoList = createAsyncThunk<TodoData[] | {}, FilterParams, { rejectValue: string }>(
  'todo/list',
  async (params, thunkAPI) => {
    try {
      const response = await TodoService.getList(params)
      if (Array.isArray(response)) {
        return response
      } else {
        return []
      }
    } catch (err: any) {
      return thunkAPI.rejectWithValue(err.response?.data?.message)
    }
  }
)

export const getTodoAll = createAsyncThunk<TodoData[] | {}, void, { rejectValue: string }>(
  'todo/all',
  async (_, thunkAPI) => {
    try {
      const response = await TodoService.getAll()
      if (Array.isArray(response)) {
        return response
      } else {
        return []
      }
    } catch (err: any) {
      return thunkAPI.rejectWithValue(err.response?.data?.message)
    }
  }
)

export const getTodoSlice = createSlice({
  name: 'get_todo',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getTodoList.pending, (state) => {
        state.todoListLoading = true
        state.todoListError = null
      })
      .addCase(getTodoList.fulfilled, (state, action) => {
        state.todoListLoading = false
        state.todoListData = action.payload
      })
      .addCase(getTodoList.rejected, (state, action) => {
        state.todoListLoading = false
        state.todoListError = action.error.message || 'Error'
      })
      .addCase(getTodoAll.pending, (state) => {
        state.todoAllLoading = true
        state.todoAllError = null
      })
      .addCase(getTodoAll.fulfilled, (state, action) => {
        state.todoAllLoading = false
        state.todoAllData = action.payload
      })
      .addCase(getTodoAll.rejected, (state, action) => {
        state.todoAllLoading = false
        state.todoAllError = action.error.message || 'Error'
      })
  }
})

export default getTodoSlice.reducer