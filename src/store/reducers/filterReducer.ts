import { createSlice } from "@reduxjs/toolkit"
import { ActiveItems, FilterParams } from "@/types"

interface IFilterInitialState {
  filterKey: ActiveItems
  filterParams: FilterParams 
}

export const initialState: IFilterInitialState = {
  filterKey: ActiveItems.All,
  filterParams: {}
}

export const filterSlice = createSlice({
  name: 'filter_todo',
  initialState,
  reducers: {
    changeFilterKey: (state, action) => {
      state.filterKey = action.payload.key
      state.filterParams = action.payload.params
    }
  },
})

export const {
  changeFilterKey
} = filterSlice.actions

export default filterSlice.reducer