import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import TodoService from "@/services/TodoService"
import type { TodoData, DeleteTodoPayload } from "@/types"

interface IDeleteTodoInitialState {
  deleteTodoLoading: boolean
  deleteTodoError: string | null
  deleteTodoData: TodoData[] | Record<PropertyKey, never>
}

export const initialState: IDeleteTodoInitialState = {
  deleteTodoLoading: false,
  deleteTodoError: null,
  deleteTodoData: []
}

export const deleteTodo = createAsyncThunk<TodoData[] | {}, DeleteTodoPayload, { rejectValue: string }>(
  'todo/delete',
  async (data, thunkAPI) => {
    try {
      const response = await TodoService.deleteTodo(data)
      if (Array.isArray(response)) {
        return response
      } else {
        return []
      }
    } catch (err: any) {
      return thunkAPI.rejectWithValue(err.response?.data?.message)
    }
  }
)

export const deleteTodoSlice = createSlice({
  name: 'delete_todo',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(deleteTodo.pending, (state) => {
        state.deleteTodoLoading = true
        state.deleteTodoError = null
      })
      .addCase(deleteTodo.fulfilled, (state, action) => {
        state.deleteTodoLoading = false
        state.deleteTodoData = action.payload
      })
      .addCase(deleteTodo.rejected, (state, action) => {
        state.deleteTodoLoading = false
        state.deleteTodoError = action.error.message || 'Error'
      })
  }
})

export default deleteTodoSlice.reducer