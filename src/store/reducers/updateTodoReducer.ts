import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import TodoService from "@/services/TodoService"
import type { TodoData, ToggleCompletePayload, UpdatePayload } from "@/types"

interface IUpdateTodoInitialState {
  toggleCompleteLoading: boolean
  updateLoading: boolean
  toggleCompleteError: string | null
  updateError: string | null
  toggleCompleteData: TodoData[] | Record<PropertyKey, never>
  updateData: TodoData[] | Record<PropertyKey, never>
}

export const initialState: IUpdateTodoInitialState = {
  toggleCompleteLoading: false,
  updateLoading: false,
  toggleCompleteError: null,
  updateError: null,
  toggleCompleteData: [],
  updateData: []
}

export const toggleComplete = createAsyncThunk<TodoData[] | {}, ToggleCompletePayload, { rejectValue: string }>(
  'todo/complete',
  async (toggleData, thunkAPI) => {
    try {
      const response = await TodoService.toggleComplete(toggleData)
      if (Array.isArray(response)) {
        return response
      } else {
        return []
      }
    } catch (err: any) {
      return thunkAPI.rejectWithValue(err.response?.data?.message)
    }
  }
)

export const updateTitle = createAsyncThunk<TodoData[] | {}, UpdatePayload, { rejectValue: string }>(
  'todo/update',
  async (data, thunkAPI) => {
    try {
      const response = await TodoService.updateTodo(data)
      if (Array.isArray(response)) {
        return response
      } else {
        return []
      }
    } catch (err: any) {
      return thunkAPI.rejectWithValue(err.response?.data?.message)
    }
  }
)

export const updateTodoSlice = createSlice({
  name: 'update_todo',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(toggleComplete.pending, (state) => {
        state.toggleCompleteLoading = true
        state.toggleCompleteError = null
      })
      .addCase(toggleComplete.fulfilled, (state, action) => {
        state.toggleCompleteLoading = false
        state.toggleCompleteData = action.payload
      })
      .addCase(toggleComplete.rejected, (state, action) => {
        state.toggleCompleteLoading = false
        state.toggleCompleteError = action.error.message || 'Error'
      })
      .addCase(updateTitle.pending, (state) => {
        state.updateLoading = true
        state.updateError = null
      })
      .addCase(updateTitle.fulfilled, (state, action) => {
        state.updateLoading = false
        state.updateData = action.payload
      })
      .addCase(updateTitle.rejected, (state, action) => {
        state.updateLoading = false
        state.updateError = action.error.message || 'Error'
      })
  }
})

export default updateTodoSlice.reducer