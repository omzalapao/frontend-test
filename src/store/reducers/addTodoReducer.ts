import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import TodoService from "@/services/TodoService"
import type { TodoData, AddTodoPayload } from "@/types"

interface IAddTodoInitialState {
  addTodoLoading: boolean
  addTodoError: string | null
  addTodoData: TodoData[] | Record<PropertyKey, never>
}

export const initialState: IAddTodoInitialState = {
  addTodoLoading: false,
  addTodoError: null,
  addTodoData: []
}

export const addTodo = createAsyncThunk<TodoData[] | {}, AddTodoPayload, { rejectValue: string }>(
  'todo/add',
  async (data, thunkAPI) => {
    try {
      const response = await TodoService.addTodo(data)
      if (Array.isArray(response)) {
        return response
      } else {
        return []
      }
    } catch (err: any) {
      return thunkAPI.rejectWithValue(err.response?.data?.message)
    }
  }
)

export const addTodoSlice = createSlice({
  name: 'add_todo',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(addTodo.pending, (state) => {
        state.addTodoLoading = true
        state.addTodoError = null
      })
      .addCase(addTodo.fulfilled, (state, action) => {
        state.addTodoLoading = false
        state.addTodoData = action.payload
      })
      .addCase(addTodo.rejected, (state, action) => {
        state.addTodoLoading = false
        state.addTodoError = action.error.message || 'Error'
      })
  }
})

export default addTodoSlice.reducer