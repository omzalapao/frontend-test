import { configureStore } from "@reduxjs/toolkit"
import { useDispatch, useSelector, TypedUseSelectorHook } from "react-redux"

import getTodoReducer from "@/store/reducers/getTodoReducer"
import updateTodoReducer from "@/store/reducers/updateTodoReducer"
import addTodoReducer from "@/store/reducers/addTodoReducer"
import deleteTodoReducer from "@/store/reducers/deleteTodoReducer"
import filterReducer from "@/store/reducers/filterReducer"

const store = configureStore({
  reducer: {
    getTodo: getTodoReducer,
    addTodo: addTodoReducer,
    updateTodo: updateTodoReducer,
    deleteTodo: deleteTodoReducer,
    filter: filterReducer
  }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export const getTodoListSelector = (state: RootState) => state.getTodo
export const updateTodoSelector = (state: RootState) => state.updateTodo
export const addTodoSelector = (state: RootState) => state.addTodo
export const deleteTodoSelector = (state: RootState) => state.deleteTodo
export const filterSelector = (state: RootState) => state.filter

export default store