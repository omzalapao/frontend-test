## Frontend Test

Install dependencies

```bash
$ npm install
```

Start Server

```bash
$ npm run dev
```

.env

```bash
$ VITE_API_HOST=http://localhost:3001
```


